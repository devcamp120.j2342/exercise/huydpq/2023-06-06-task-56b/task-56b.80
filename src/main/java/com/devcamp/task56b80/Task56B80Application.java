package com.devcamp.task56b80;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56B80Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56B80Application.class, args);
	}

}
