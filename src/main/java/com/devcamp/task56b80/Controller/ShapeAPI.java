package com.devcamp.task56b80.Controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56b80.models.Circle;
import com.devcamp.task56b80.models.Rectangle;
import com.devcamp.task56b80.models.Square;

@RestController
public class ShapeAPI {
    @CrossOrigin
    //diện tích hình tròn.
    @GetMapping("/circle-area")
    public double circleArea() {
        Circle circle = new Circle(3.0);
        return circle.getArea();
    }

    //chu vi hình tròn
    @GetMapping("/circle-perimeter")
    public double circlePerimeter() {
        Circle circle1 = new Circle(3.0);
        return circle1.getPerimeter();
    }

    //diện tích hình chữ nhật.
    @GetMapping("/rectangle-area")
    public double rectangleArea(
        @RequestParam(value = "width" , defaultValue = "1.0") double width,
        @RequestParam(value = "height" , defaultValue = "1.0") double height ) {

        Rectangle rectangle = new Rectangle(width, height);
        return rectangle.getArea();
        
    }
    //chu vi hình chữ nhật.
    @GetMapping("/rectangle-perimeter")
    public double rectanglePerimeter(
        @RequestParam(value = "width" , defaultValue = "1.0") double width,
        @RequestParam(value = "height" , defaultValue = "1.0") double height ) {

        Rectangle rectangle = new Rectangle(width, height);
        return rectangle.getPerimeter();
        
    }
    // diện tích hình vuông
    @GetMapping("/square-area")
    public double squareArea(
        @RequestParam(value = "side" , defaultValue = "1.0") double abc) {

        Square rectangle = new Square(abc);
        return rectangle.getArea();
        
    }

    // chu vi hình vuông
    @GetMapping("/square-perimeter")
    public double squarePerimeter(
        @RequestParam(value = "side" , defaultValue = "1.0") double side) {

        Square rectangle = new Square(side);
        return rectangle.getPerimeter();
        
    }
}
